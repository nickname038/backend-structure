const joi = require("joi");

const schema = joi
  .object({
    score: joi.string().required(),
  })
  .required();

module.exports = schema;
