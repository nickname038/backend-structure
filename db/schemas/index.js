let betPostSchema = require("./bet/bet.post");
let eventPostSchema = require("./event/event.post");
let eventPutSchema = require("./event/event.put");
let transactionPostSchema = require("./transaction/transaction.post");
let userGetSchema = require("./user/user.get");
let userPostSchema = require("./user/user.post");
let userPutSchema = require("./user/user.put");

module.exports = {
  betPostSchema,
  eventPostSchema,
  eventPutSchema,
  transactionPostSchema,
  userGetSchema,
  userPostSchema,
  userPutSchema,
};
