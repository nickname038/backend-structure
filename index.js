const express = require("express");

const app = express();
const port = 3000;

app.use(express.json());

const routes = require("./routes/index");
routes(app);

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };
