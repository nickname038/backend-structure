const jwt = require("jsonwebtoken");

const authorizationMiddlewareSimple = (req, res, next) => {
  let token = req.headers["authorization"];
  if (!token) {
    res.status(401);
    res.err = "Not Authorized";
  } else {
    token = token.replace("Bearer ", "");
    try {
      const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
      req.user_id = tokenPayload.id;
    } catch (err) {
      res.status(401);
      res.err = "Not Authorized";
    }
  }

  next();
};

const authorizationMiddlewareAdmin = (req, res, next) => {
  let token = req.headers["authorization"];
  if (!token) {
    res.status(401);
    res.err = "Not Authorized";
  } else {
    token = token.replace("Bearer ", "");
    try {
      var tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
      if (tokenPayload.type != "admin") {
        throw new Error();
      }
    } catch (err) {
      res.status(401);
      res.err = "Not Authorized";
    }
  }
  next();
};

module.exports = {
  authorizationMiddlewareSimple,
  authorizationMiddlewareAdmin,
};
