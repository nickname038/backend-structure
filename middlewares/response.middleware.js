const responseMiddleware = (req, res, next) => {
  if (res.err) {
    res.json({ error: res.err });
  } else {
    res.status(200).json(res.data);
  }
  next();
};

exports.responseMiddleware = responseMiddleware;
