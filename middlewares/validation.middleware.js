const {
  betPostSchema,
  eventPostSchema,
  eventPutSchema,
  transactionPostSchema,
  userGetSchema,
  userPostSchema,
  userPutSchema,
} = require("./../db/schemas/index");

const requestValid = (schema) => (req, res, next) => {
  if (!res.err) {
    const result =
      req.body && Object.keys(req.body).length ? req.body : req.params;
    const isValidResult = schema.validate(result);
    if (isValidResult.error) {
      res.status(400);
      res.err = isValidResult.error.details[0].message;
    }
  }
  next();
};

const betPostValid = requestValid(betPostSchema);
const eventPostValid = requestValid(eventPostSchema);
const eventPutValid = requestValid(eventPutSchema);
const transactionPostValid = requestValid(transactionPostSchema);
const userGetValid = requestValid(userGetSchema);
const userPostValid = requestValid(userPostSchema);
const userPutValid = requestValid(userPutSchema);

module.exports = {
  betPostValid,
  eventPostValid,
  eventPutValid,
  transactionPostValid,
  userGetValid,
  userPostValid,
  userPutValid,
};
