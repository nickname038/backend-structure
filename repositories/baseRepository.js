const { db } = require("./../dbInitialisation");

const duplicateEmailErrorCode = "23505";

class BaseRepository {
  constructor(collectionName) {
    this.collectionName = collectionName;
    this.db = db;
  }

  async getById(id) {
    return db(this.collectionName)
      .where("id", id)
      .returning("*")
      .then(([result]) => {
        if (!result) return null;
        return result;
      });
  }

  async create(data) {
    return db(this.collectionName)
      .insert(data)
      .returning("*")
      .then(([result]) => {
        result.createdAt = result.created_at;
        delete result.created_at;
        result.updatedAt = result.updated_at;
        delete result.updated_at;
        return result;
      });
  }

  async update(id, dataToUpdate) {
    return db(this.collectionName)
      .where("id", id)
      .update(dataToUpdate)
      .returning("*")
      .then(([result]) => {
        return result;
      });
  }
}

exports.BaseRepository = BaseRepository;
