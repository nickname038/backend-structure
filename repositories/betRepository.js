const { BaseRepository } = require("./baseRepository");

class BetRepository extends BaseRepository {
  constructor() {
    super("bet");
  }

  getBets({ eventId, win }) {
    return this.db("bet").where("event_id", eventId).andWhere("win", win);
  }
}

exports.BetRepository = new BetRepository();
