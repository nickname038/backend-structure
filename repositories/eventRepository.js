const { BaseRepository } = require("./baseRepository");

class EventRepository extends BaseRepository {
  constructor() {
    super("event");
  }
}

exports.EventRepository = new EventRepository();
