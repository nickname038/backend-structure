const { BaseRepository } = require("./baseRepository");

class OddsRepository extends BaseRepository {
  constructor() {
    super("odds");
  }
}

exports.OddsRepository = new OddsRepository();
