const { Router } = require("express");
const { HttpError } = require("./../helpers/httpError");
const { betPostValid } = require("../middlewares/validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  authorizationMiddlewareSimple,
} = require("../middlewares/authorization.middleware");
const BetService = require("./../services/betService");

const router = Router();

router.post(
  "/",
  authorizationMiddlewareSimple,
  betPostValid,
  async (req, res, next) => {
    if (!res.err) {
      try {
        req.body.event_id = req.body.eventId;
        req.body.bet_amount = req.body.betAmount;
        delete req.body.eventId;
        delete req.body.betAmount;
        req.body.user_id = req.user_id;

        const result = await BetService.create(req.body);
        res.data = result;
      } catch (err) {
        if (err instanceof HttpError) {
          res.status(err.code);
          res.err = err.message;
        } else {
          res.status(500);
          res.err = "Internal Server Error";
        }
      }
    }

    next();
  },

  responseMiddleware
);

module.exports = router;
