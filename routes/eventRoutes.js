const { Router } = require("express");
const {
  eventPostValid,
  eventPutValid,
} = require("../middlewares/validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  authorizationMiddlewareAdmin,
} = require("../middlewares/authorization.middleware");
const router = Router();
const EventService = require("./../services/eventService");

router.post(
  "/",
  authorizationMiddlewareAdmin,
  eventPostValid,
  async (req, res, next) => {
    if (!res.err) {
      try {
        const result = await EventService.create(req.body);
        res.data = result;
      } catch (err) {
        res.status(500);
        res.err = "Internal Server Error";
      }
    }
    next();
  },
  responseMiddleware
);

router.put(
  "/:id",
  authorizationMiddlewareAdmin,
  eventPutValid,
  async (req, res, next) => {
    if (!res.err) {
      try {
        const result = await EventService.update(req.params.id, req.body);
        res.data = result;
      } catch (err) {
        res.status(500);
        res.err = "Internal Server Error";
        next();
      }
    }
    next();
  },
  responseMiddleware
);

module.exports = router;
