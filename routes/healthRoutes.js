const { Router } = require("express");
const { responseMiddleware } = require("../middlewares/response.middleware");
const router = Router();

router.get(
  "/",
  (req, res, next) => {
    res.data = "Hello World!";
    next();
  },
  responseMiddleware
);

module.exports = router;
