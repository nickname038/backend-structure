const betRoutes = require("./betRoutes");
const eventRoutes = require("./eventRoutes");
const statsRoutes = require("./statsRoutes");
const transactionRoutes = require("./transactionRoutes");
const userRoutes = require("./userRoutes");
const healthRoutes = require("./healthRoutes");

module.exports = (app) => {
  app.use("/users", userRoutes);
  app.use("/transactions", transactionRoutes);
  app.use("/health", healthRoutes);
  app.use("/events", eventRoutes);
  app.use("/bets", betRoutes);
  app.use("/stats", statsRoutes);
};
