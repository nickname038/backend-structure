const { Router } = require("express");
const { responseMiddleware } = require("../middlewares/response.middleware");
const router = Router();
const { authorizationMiddlewareAdmin } = require("../middlewares/authorization.middleware");

const { stats } = require("./../statsEmitter");

router.get(
  "/",
  authorizationMiddlewareAdmin,
  (req, res, next) => {
    try {
      res.data = stats;
    } catch (err) {
      res.status(500);
      res.err = "Internal Server Error";
    }
    next();
  },
  responseMiddleware
);

module.exports = router;
