const { Router } = require("express");
const { HttpError } = require("./../helpers/httpError");
const {
  transactionPostValid,
} = require("../middlewares/validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");
const TransactionService = require("../services/transactionService");
const router = Router();
const {
  authorizationMiddlewareAdmin,
} = require("../middlewares/authorization.middleware");

router.post(
  "/",
  authorizationMiddlewareAdmin,
  transactionPostValid,
  async (req, res, next) => {
    if (!res.err) {
      try {
        const result = await TransactionService.create(req.body);
        res.data = result;
      } catch (err) {
        if (err instanceof HttpError) {
          res.status(err.code);
          res.err = err.message;
        } else {
          res.status(500);
          res.err = "Internal Server Error";
        }
      }
    }
    next();
  },
  responseMiddleware
);

module.exports = router;
