const { Router } = require("express");
const {
  userGetValid,
  userPostValid,
  userPutValid,
} = require("../middlewares/validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  authorizationMiddlewareSimple,
} = require("../middlewares/authorization.middleware");
const UserService = require("./../services/userService");
const { HttpError } = require("../helpers/httpError");

const router = Router();

router.get(
  "/:id",
  userGetValid,
  async (req, res, next) => {
    if (!res.err) {
      try {
        const result = await UserService.search(req.params.id);
        res.data = result;
      } catch (err) {
        if (err instanceof HttpError) {
          res.status(err.code);
          res.err = err.message;
        } else {
          res.status(500);
          res.err = "Internal Server Error";
        }
      }
    }
    next();
  },
  responseMiddleware
);

router.post(
  "/",
  userPostValid,
  async (req, res, next) => {
    if (!res.err) {
      try {
        const result = await UserService.create(req.body);
        res.data = result;
      } catch (err) {
        if (err instanceof HttpError) {
          res.status(err.code);
          res.err = err.message;
        } else {
          res.status(500);
          res.err = "Internal Server Error";
        }
      }
    }
    next();
  },
  responseMiddleware
);

router.put(
  "/:id",
  authorizationMiddlewareSimple,
  userPutValid,
  async (req, res, next) => {
    if (!res.err) {
      if (req.params.id !== req.user_id) {
        res.status(401);
        res.err = "UserId mismatch";
        next();
        return;
      }

      try {
        const result = await UserService.update(req.params.id, req.body);
        res.data = result;
      } catch (err) {
        if (err instanceof HttpError) {
          res.status(err.code);
          res.err = err.message;
        } else {
          res.status(500);
          res.err = "Internal Server Error";
        }
      }
    }

    next();
  },
  responseMiddleware
);

module.exports = router;
