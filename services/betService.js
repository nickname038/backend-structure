const { HttpError } = require("./../helpers/httpError");
const DUPLICATE_EMAIL_ERROR_CODE = "23505";
const { statEmitter } = require("./../statsEmitter");

const { UserRepository } = require("./../repositories/userRepository");
const { EventRepository } = require("./../repositories/eventRepository");
const { OddsRepository } = require("./../repositories/oddsRepository");
const { BetRepository } = require("./../repositories/betRepository");

const multiplierPredictions = {
  w1: "home_win",
  w2: "away_win",
  x: "draw",
};

class BetService {
  async create(data) {
    try {
      let user = await UserRepository.getById(data.user_id);
      if (!user) {
        throw new HttpError("User does not exist", 400);
      }
      if (+user.balance < +data.bet_amount) {
        throw new HttpError("Not enough balance", 400);
      }

      const event = await EventRepository.getById(data.event_id);
      if (!event) {
        throw new HttpError("Event not found", 404);
      }

      const odds = await OddsRepository.getById(event.odds_id);
      if (!odds) {
        throw new HttpError("Odds not found", 404);
      }

      const multiplier = odds[multiplierPredictions[data.prediction]];

      const bet = await BetRepository.create({
        ...data,
        multiplier,
        event_id: event.id,
      });
      const currentBalance = user.balance - data.bet_amount;

      user = await UserRepository.update(data.user_id, {
        balance: currentBalance,
      });

      statEmitter.emit("newBet");

      [
        "bet_amount",
        "event_id",
        "away_team",
        "home_team",
        "odds_id",
        "start_at",
        "user_id",
      ].forEach((key) => {
        var index = key.indexOf("_");
        var newKey = key.replace("_", "");
        newKey = newKey.split("");
        newKey[index] = newKey[index].toUpperCase();
        newKey = newKey.join("");
        bet[newKey] = bet[key];
        delete bet[key];
      });

      return {
        ...bet,
        currentBalance: currentBalance,
      };
    } catch (err) {
      if (err.code == DUPLICATE_EMAIL_ERROR_CODE) {
        throw new HttpError(err.detail, 400);
      }
      throw err;
    }
  }
}

module.exports = new BetService();
