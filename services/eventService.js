const { statEmitter } = require("./../statsEmitter");

const { UserRepository } = require("./../repositories/userRepository");
const { EventRepository } = require("./../repositories/eventRepository");
const { OddsRepository } = require("./../repositories/oddsRepository");
const { BetRepository } = require("./../repositories/betRepository");

class EventService {
  async create(data) {
    data.odds.home_win = data.odds.homeWin;
    delete data.odds.homeWin;
    data.odds.away_win = data.odds.awayWin;
    delete data.odds.awayWin;

    const odds = await OddsRepository.create(data.odds);
    delete data.odds;
    data.away_team = data.awayTeam;
    data.home_team = data.homeTeam;
    data.start_at = data.startAt;
    delete data.awayTeam;
    delete data.homeTeam;
    delete data.startAt;

    const event = await EventRepository.create({
      ...data,
      odds_id: odds.id,
    });
    statEmitter.emit("newEvent");
    [
      "bet_amount",
      "event_id",
      "away_team",
      "home_team",
      "odds_id",
      "start_at",
    ].forEach((key) => {
      var index = key.indexOf("_");
      var newKey = key.replace("_", "");
      newKey = newKey.split("");
      newKey[index] = newKey[index].toUpperCase();
      newKey = newKey.join("");
      event[newKey] = event[key];
      delete event[key];
    });

    ["home_win", "away_win"].forEach((key) => {
      var index = key.indexOf("_");
      var newKey = key.replace("_", "");
      newKey = newKey.split("");
      newKey[index] = newKey[index].toUpperCase();
      newKey = newKey.join("");
      odds[newKey] = odds[key];
      delete odds[key];
    });

    return {
      ...event,
      odds,
    };
  }

  async update(eventId, data) {
    const bets = await BetRepository.getBets({ eventId, win: null });
    var [w1, w2] = data.score.split(":");
    let result;
    if (+w1 > +w2) {
      result = "w1";
    } else if (+w2 > +w1) {
      result = "w2";
    } else {
      result = "x";
    }

    const event = await EventRepository.update(eventId, {
      score: data.score,
    });

    Promise.all(
      bets.map(async (bet) => {
        if (bet.prediction == result) {
          await BetRepository.update(bet.id, {
            win: true,
          });

          const user = await UserRepository.getById(bet.user_id);
          return await UserRepository.update(bet.user_id, {
            balance: user.balance + bet.bet_amount * bet.multiplier,
          });
        } else if (bet.prediction != result) {
          return await BetRepository.update(bet.id, {
            win: false,
          });
        }
      })
    );

    return new Promise((resolve) => {
      setTimeout(() => {
        [
          "bet_amount",
          "event_id",
          "away_team",
          "home_team",
          "odds_id",
          "start_at",
          "created_at",
          "updated_at",
        ].forEach((key) => {
          var index = key.indexOf("_");
          var newKey = key.replace("_", "");
          newKey = newKey.split("");
          newKey[index] = newKey[index].toUpperCase();
          newKey = newKey.join("");
          event[newKey] = event[key];
          delete event[key];
        });
        resolve(event);
      }, 1000);
    });
  }
}

module.exports = new EventService();
