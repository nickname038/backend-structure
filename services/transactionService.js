const { HttpError } = require("./../helpers/httpError");

const { UserRepository } = require("./../repositories/userRepository");
const { TransactionRepository } = require("./../repositories/transactionRepository");

class TransactionService {
  async create(data) {
    let user = await UserRepository.getById(data.userId);
    if (!user) {
      throw new HttpError("User does not exist", 400);
    }

    data.card_number = data.cardNumber;
    delete data.cardNumber;
    data.user_id = data.userId;
    delete data.userId;

    const transaction = await TransactionRepository.create(data);
    var currentBalance = data.amount + user.balance;

    user = await UserRepository.update(data.user_id, {
      balance: currentBalance,
    });

    ["user_id", "card_number"].forEach((key) => {
      var index = key.indexOf("_");
      var newKey = key.replace("_", "");
      newKey = newKey.split("");
      newKey[index] = newKey[index].toUpperCase();
      newKey = newKey.join("");
      transaction[newKey] = transaction[key];
      delete transaction[key];
    });

    return {
      ...transaction,
      currentBalance,
    };
  }
}

module.exports = new TransactionService();
