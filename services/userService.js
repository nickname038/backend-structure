const { UserRepository } = require("./../repositories/userRepository");
const { HttpError } = require("./../helpers/httpError");
const DUPLICATE_EMAIL_ERROR_CODE = "23505";
const { statEmitter } = require("./../statsEmitter");
const jwt = require("jsonwebtoken");

class UserService {
  async search(id) {
    const result = await UserRepository.getById(id);
    if (!result) {
      throw new HttpError("User not found", 404);
    }
    return { ...result };
  }

  async create(data) {
    try {
      data.balance = 0;
      const result = await UserRepository.create(data);
      statEmitter.emit("newUser");
      return {
        ...result,
        accessToken: jwt.sign(
          { id: result.id, type: result.type },
          process.env.JWT_SECRET
        ),
      };
    } catch (err) {
      if (err.code == DUPLICATE_EMAIL_ERROR_CODE) {
        throw new HttpError(err.detail, 400);
      }
      throw err;
    }
  }

  async update(id, dataToUpdate) {
    try {
      const result = await UserRepository.update(id, dataToUpdate);
      return {
        ...result,
      };
    } catch (err) {
      if (err.code == DUPLICATE_EMAIL_ERROR_CODE) {
        throw new HttpError(err.detail, 400);
      }
      throw err;
    }
  }
}

module.exports = new UserService();
